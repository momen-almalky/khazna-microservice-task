const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger_docs.json");
require("./src/db/models/index");
const errorHandler = require("./src/middleware/errorHandler");
const user_router = require("./src/routers/user");
const product_router = require("./src/routers/product");
const order_router = require("./src/routers/order");
const queue = require("./src/helpers/queue");

dotenv.config();

const app = express();
const server = require("http").createServer(app);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(user_router);
app.use(product_router);
app.use(order_router);

// global error handler
app.use(errorHandler);

console.log(
  "Open http://localhost:3000/api-docs to explore swagger and make requests"
);

const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log("Server is up on port " + port);
});

const intervalId = setInterval(() => {
  queue.subscribeToOrderQueue().then((isConnected) => {
    if (isConnected) {
      clearInterval(intervalId);
      console.log("OrderQueue is up:");
    }
  });
}, 20000);

module.exports = server;
