const Joi = require("joi");
const constants = require("../helpers/constants");

const new_user_validation = (data) => {
  const schema = Joi.object({
    email: Joi.string().min(6).email().required(),
    password: Joi.string().min(1).required(),
    balance: Joi.number().required(),
  });

  return schema.validate(data);
};

const login_validation = (data) => {
  const schema = Joi.object({
    email: Joi.string().min(6).email().required(),
    password: Joi.string().min(1).required(),
  });

  return schema.validate(data);
};

const balance_action_validation = (data) => {
  const schema = Joi.object({
    user_id: Joi.number().required(),
    amount: Joi.number().required(),
    action: Joi.string()
      .min(1)
      .valid(constants.balanceDeductAction, constants.balanceAddAction),
  });

  return schema.validate(data);
};
module.exports.new_user_validation = new_user_validation;
module.exports.login_validation = login_validation;
module.exports.balance_action_validation = balance_action_validation;
