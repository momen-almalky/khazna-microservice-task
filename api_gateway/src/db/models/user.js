const { Model } = require("sequelize");
require("dotenv");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    toJSON() {
      const userObj = Object.assign({}, this.dataValues);
      delete userObj.password;
      delete userObj.token;
      return userObj;
    }

    async generateAuthToken() {
      const user = Object.assign({}, this.dataValues);
      if (user) {
        const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET);
        return token;
      }
    }

    async comparePassword(password) {
      const isMatch = await bcrypt.compare(password, this.password);
      return isMatch;
    }
  }

  User.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      token: DataTypes.STRING,
      balance: {
        type: DataTypes.DECIMAL(15, 2),
        get() {
          const rawValue = this.getDataValue('balance');
          return +rawValue;
        },
      },
    },
    {
      hooks: {
        beforeCreate: async (user, options) => {
          if (user.changed("password")) {
            user.password = await bcrypt.hash(user.password, 10);
          }
        },
      },
      sequelize,
      modelName: "User",
    }
  );

  return User;
};
