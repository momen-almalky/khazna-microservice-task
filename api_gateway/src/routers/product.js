const express = require("express");
require("dotenv");
const userauth = require("../middleware/userauth");
const productService = require("./services/product_service");
const errorHandler = require("../middleware/errorHandler");

const router = new express.Router();

/**
 * @api {post} /products/ Get list of products based on filters
 * @apiName List products
 * @apiGroup products
 *
 * @apiParam  {object} filters
 * @apiParam  {Integer} filters.category_id
 * @apiParam  {Integer} filters.brand_id
 *
 * @apiSuccess (200) {Array} mixed `{Products}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/products", userauth, async (req, res, next) => {
  try {
    const { body } = req;
    body.user = req.user;

    let products = await productService.getList(body);

    res.status(200).send({ products: products });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

module.exports = router;
