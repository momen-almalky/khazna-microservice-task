const express = require("express");
const {
  new_user_validation,
  login_validation,
  balance_action_validation,
} = require("../validations/user_validation");
require("dotenv");
const userauth = require("../middleware/userauth");
const userService = require("./services/user_service");
const errorHandler = require("../middleware/errorHandler");
const CustomError = require("../helpers/CustomError");
const constants = require("../helpers/constants");

const router = new express.Router();

/**
 * @api {post} /users/register Register User
 * @apiName Register user
 * @apiGroup users
 *
 * @apiParam  {String} Email
 * @apiParam  {String} Password
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/users/register", async (req, res, next) => {
  try {
    const { body } = req;
    const { error } = new_user_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const user = await userService.register(body);

    res.status(200).send({ user: user });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {post} /users/register Login User
 * @apiName Login user
 * @apiGroup users
 *
 * @apiParam  {String} Email
 * @apiParam  {String} Password
 *
 * @apiSuccess (200) {Sting} token
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/users/login", async (req, res, next) => {
  try {
    const { body } = req;

    const { error } = login_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const token = await userService.login(body);
    res.status(200).send({ token: token });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {post} /users/profile User Profile
 * @apiName User Profile
 * @apiGroup users
 *
 * @apiParam  {String} token
 *
 * @apiSuccess (200) {Object} mixed `User` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/users/profile", userauth, async (req, res, next) => {
  try {
    res.status(200).send({ user: req.user });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {post} /users/balance Balance
 * @apiName Balance
 * @apiGroup users
 *
 * @apiParam  {String} token
 *
 * @apiSuccess (200) {string} string `balance`
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/users/balance", userauth, async (req, res, next) => {
  try {
    const { body } = req;

    const { error } = balance_action_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const new_balance = await userService.balanceAction(body);

    res.status(200).send({ success: new_balance });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});
module.exports = router;
