require("dotenv");
const constants = require("../../helpers/constants");
const CustomError = require("../../helpers/CustomError");
const axios = require("axios").default;

/**
 * getList
 * get List of Products
 * @param {Object} user
 * @param {Integer} category_id
 * @param {Integer} brand_id
 * @return {Array} Products
 */
module.exports.getList = async function ({ user, category_id, brand_id }) {
  const params = {
    price: user.balance,
    category_id: category_id,
    brand_id: brand_id,
  };

  const response = await axios.post(
    `${process.env.PRODUCT_SERVICE_URL}/products`,
    {
      filters: params,
    }
  );

  if (response.status != 200 || !response.data) {
    throw new CustomError(constants.string, "Can't get product list");
  }

  return response.data.products;
};
