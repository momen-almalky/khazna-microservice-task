require("dotenv");
const constants = require("../../helpers/constants");
const CustomError = require("../../helpers/CustomError");
const axios = require("axios").default;

/**
 * Create Order
 * create order by user_id and products
 * @param {Object} user
 * @param {Array} products
 * @return {Object} Order
 */
module.exports.create = async function ({ user, products }) {
  try {
    const response = await axios.post(
      `${process.env.ORDER_SERVICE_URL}/orders`,
      {
        user_id: user.id,
        products: products,
      }
    );

    return { status: response.status, data: response.data };
  } catch (error) {
    return { status: error.response.status, data: error.response.data };
  }
};

/**
 * getList
 * get List of orders
 * @param {Integer} user_id
 * @return {Array} Orders
 */
module.exports.getList = async function ({ id }) {
  try {
    const response = await axios.get(
      `${process.env.ORDER_SERVICE_URL}/orders`,
      {
        params: {
          user_id: id,
        },
      }
    );

    return { status: response.status, data: response.data };
  } catch (error) {
    return { status: error.response.status, data: error.response.data };
  }
};

/**
 * Cancel Order
 * cancel order by user_id and order_id
 * @param {Object} user
 * @param {Array} order_id
 * @return {Object} Order
 */
module.exports.cancel = async function ({ order_id }) {
  try {
    const response = await axios.post(
      `${process.env.ORDER_SERVICE_URL}/orders/cancel`,
      {
        order_id: order_id,
      }
    );

    return { status: response.status, data: response.data };
  } catch (error) {
    return { status: error.response.status, data: error.response.data };
  }
};
