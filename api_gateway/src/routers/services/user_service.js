const { User } = require("../../db/models");
require("dotenv");
const constants = require("../../helpers/constants");
const CustomError = require("../../helpers/CustomError");
const sequelize = require("../../db/models").sequelize;
const queue = require("../../helpers/queue");

/**
 * register
 * create new user
 * @param {String} email
 * @param {String} password
 * @return {Object} User
 */
module.exports.register = async function ({ email, password, balance }) {
  if (await User.findOne({ where: { email: email } })) {
    throw new CustomError(constants.string, "this email used before");
  }

  const user = User.build({
    email: email,
    password: password,
    balance: balance,
  });
  await user.save();

  return user.toJSON();
};

/**
 * login
 * check if user found and check password
 * @param {String}  email
 * @param {String} password
 * @return {String} token
 */
module.exports.login = async function ({ email, password }) {
  user = await User.findOne({ where: { email: email } });
  if (!user) {
    throw new CustomError(constants.NotFound, "User not found");
  }

  const isMatch = await user.comparePassword(password);
  if (!isMatch) {
    throw new CustomError(constants.string, "Wrong credentials");
  }

  const token = await user.generateAuthToken();

  user.token = token;
  await user.save();

  return token;
};

/**
 * balanceAction
 * deduct or add an amount to user balance based on action
 * @param {String}  user_id
 * @param {String}  action
 * @param {String} amount
 * @return {String} token
 */
module.exports.balanceAction = async function ({ user_id, action, amount }) {
  const transaction = await sequelize.transaction();

  user = await User.findOne({
    where: { id: user_id },
    lock: transaction.LOCK.SHARE,
  });

  if (!user) {
    throw new CustomError(constants.NotFound, "User not found");
  }

  if (action == constants.balanceDeductAction) {
    amount = -amount;
  }

  const balance_after_action = user.balance + +amount;
  if (balance_after_action < 0) {
    throw new CustomError(constants.string, `No enough balance`);
  }

  try {
    user.balance = balance_after_action;
    user.save();

    if (user.balance < 0) {
      throw new CustomError(constants.string, `No enough balance`);
    }
    await transaction.commit();

    return true;
  } catch {
    await transaction.rollback();
    throw new CustomError(
      constants.string,
      `Couldn't perform ${action} amount ${amount}on user ${user.email}`
    );
  }
};
