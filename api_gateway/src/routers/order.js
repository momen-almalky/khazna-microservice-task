const express = require("express");
require("dotenv");
const userauth = require("../middleware/userauth");
const orderService = require("./services/order_service");
const errorHandler = require("../middleware/errorHandler");

const router = new express.Router();

/**
 * @api {post} /orders/ Create new Order
 * @apiName Create Order
 * @apiGroup orders
 *
 * @apiParam  {Array} products
 * @apiParam  {Integer} user_id
 *
 * @apiSuccess (200) {Object} mixed `{Order}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/orders", userauth, async (req, res, next) => {
  try {
    const { body } = req;
    body.user = req.user;

    let response = await orderService.create(body);

    res.status(response.status).send(response.data);
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {get} /orders/ Get list of orders
 * @apiName List orders
 * @apiGroup orders
 *
 * @apiParam  {object} user_id
 *
 * @apiSuccess (200) {Array} mixed `{Orders}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/orders", userauth, async (req, res, next) => {
  try {
    const { user } = req;
    let response = await orderService.getList(user);

    res.status(response.status).send(response.data);
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {post} /orders/cancel Cancel Order
 * @apiName Cancel Order
 * @apiGroup orders
 *
 * @apiParam  {Integer} user_id
 * @apiParam  {Integer} order_id
 *
 * @apiSuccess (200) {Object} mixed `{Order}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/orders/cancel", userauth, async (req, res, next) => {
  try {
    const { body } = req;
    body.user = req.user;

    let response = await orderService.cancel(body);

    res.status(response.status).send(response.data);
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

module.exports = router;
