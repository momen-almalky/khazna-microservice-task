module.exports = {
  NotFound: "NotFound",
  string: "string",
  UnauthorizedError: "UnauthorizedError",
  UnprocessEntity: "UnprocessEntity",
  balanceDeductAction: "deduct",
  balanceAddAction: "add",

  orderCreatedEvent: "order_created_event",
  orderCancelledEvent: "order_cancelled_event",
  paymentApprovedEvent: "payment_approved_event",
  paymentDeclinedEvent: "payment_declined_event",
};
