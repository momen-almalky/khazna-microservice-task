const amqp = require("amqplib");
const EventEmitter = require("events");
require("dotenv");
const constants = require("../helpers/constants");
const userService = require("../routers/services/user_service");
const CustomError = require("../helpers/CustomError");

const produce = async (queue, message, durable = false, persistent = false) => {
  const connect = await amqp.connect(process.env.RABBIT_MQ_URL);
  const channel = await connect.createChannel();

  await channel.assertQueue(queue, { durable });
  await channel.sendToQueue(queue, Buffer.from(message), { persistent });
};

const consume = async (
  queue,
  isNoAck = false,
  durable = false,
  prefetch = null
) => {
  const connect = await amqp.connect(process.env.RABBIT_MQ_URL);
  const channel = await connect.createChannel();

  await channel.assertQueue(queue, { durable });

  if (prefetch) {
    channel.prefetch(prefetch);
  }
  const consumeEmitter = new EventEmitter();
  try {
    channel.consume(
      queue,
      (message) => {
        if (message !== null) {
          content = JSON.parse(message.content.toString());
          consumeEmitter.emit(content.event, content.data, () =>
            channel.ack(message)
          );
        } else {
          const error = new CustomError(
            constants.string,
            "NullMessageException"
          );
          consumeEmitter.emit("error", error);
        }
      },
      { noAck: isNoAck }
    );
  } catch (error) {
    consumeEmitter.emit("error", error);
  }
  return consumeEmitter;
};

const subscribeToOrderQueue = async () => {
  try {
    const consumeEmmitter = await consume(process.env.USER_QUEUE);

    consumeEmmitter.on(constants.orderCreatedEvent, async (order, ack) => {
      let deduct_flag = false;

      try {
        deduct_flag = await userService.balanceAction({
          user_id: order.user_id,
          action: constants.balanceDeductAction,
          amount: order.total,
        });

        if (!deduct_flag) {
          await produce(
            process.env.ORDERS_QUEUE,
            JSON.stringify({ event: constants.paymentDeclined, data: order })
          );
          return;
        }

        await produce(
          process.env.ORDERS_QUEUE,
          JSON.stringify({
            event: constants.paymentApprovedEvent,
            data: order,
          })
        );
        return;
      } catch (error) {
        console.log(error);
      }

      if (!deduct_flag) {
        await produce(
          process.env.ORDERS_QUEUE,
          JSON.stringify({ event: constants.paymentDeclinedEvent, data: order })
        );
      }
    });

    consumeEmmitter.on(constants.orderCancelledEvent, async (order, ack) => {
      await userService.balanceAction({
        user_id: order.user_id,
        action: constants.balanceAddAction,
        amount: order.total,
      });
    });
    consumeEmmitter.on("error", (error) => {
      console.log(error);
    });
    return true;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  produce,
  consume,
  subscribeToOrderQueue,
};
