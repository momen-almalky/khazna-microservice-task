const chai = require("chai");
const expect = chai.expect;
const register_user_sample = require("../samples/register_user_sample");
const invalid_register_user_sample = require("../samples/invalid_register_user_sample");
const login_user_sample = require("../samples/login_user_sample");
const invalid_login_user_sample = require("../samples/invalid_login_user_sample");
const request = require("supertest");
database = require("../../src/db/models/index");
server = require("../../index");

describe("user router", () => {

  before((done) => {
    require("../../src/db/models/index");
    done();
  })

  describe("POST /users/register", () => {
    it("should create user in database", async () => {
      const res = await request(server).post("/users/register").send(register_user_sample);
      expect(res.status).eql(200);
    });
  });

  describe("POST /users/login", () => {
    it("should return 200", async () => {
      const res = await request(server).post("/users/login").send(login_user_sample);
      expect(res.status).eql(200);
    });
  });
  
  describe("POST /users/register", () => {
    it("should return unprocess entity 422", async () => {
      const res = await request(server).post("/users/register").send(invalid_register_user_sample);
      expect(res.status).eql(422);
    });
  });

  describe("POST /users/login", () => {
    it("should return unprocess entity 422", async () => {
      const res = await request(server).post("/users/login").send(invalid_login_user_sample);
      expect(res.status).eql(422);
    });
  });
});
