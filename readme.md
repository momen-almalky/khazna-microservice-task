# Khazna Test Task

This is a test task that implement an app consist of three Microservices that do the following<br />
 1- login / register <br />
 2- view / filter products from list <br />
 3- request products <br />
 4- check his previous requests <br />
 5- cancel requests if it is not delivered<br />

## Outline

 - [Libraries](#Libraries)
 - [Installation](#installation)
 - [Dependencies](#Dependencies)
 - [Swagger](#Swagger)
 - [Architecture](#Architecture)
 - [Steps to run tests](#Test)

## Libraries

This project is using several libraries and frameworks:

 - [sequelize](https://www.npmjs.com/package/sequelize) - Database
 - [amqplib](https://www.npmjs.com/package/amqplib) - RabbitMQ Client
 - [axios](https://www.npmjs.com/package/axios) - Promise based HTTP client 
 - [bcryptjs](https://www.npmjs.com/package/bcryptjs) - Store hash in your password DB 
 - [jsonwebtoken](https://www.npmjs.com/package/axios) - Generate tokens for users 
 - [joi](https://www.npmjs.com/package/joi) - Joi Validator
 - [mocha](https://www.npmjs.com/package/mocha) - Testing
 - [chai](https://www.npmjs.com/package/chai) - Testing

## Installation

### Dependencies

Make sure you have Docker:

To run project 

```bash
$ docker-compose build
```

Then type into the terminal:

```bash
$ docker-compose up
```


Then after you see that the mysql is connected to port 3306 run the following:
```bash
docker-compose run api-gateway npm run dbcreate
docker-compose run api-gateway npm run dbmigrate

docker-compose run order-service npm run dbcreate
docker-compose run order-service npm run dbmigrate

docker-compose run product-service npm run dbcreate
docker-compose run product-service npm run dbmigrate
docker-compose run product-service npm run dbseed
```
Start using APIs on swagger steps:<br />

1- Open Swagger http://localhost:3000/api-docs  <br />

2- Register user first by using API POST method /users/register<br />

3- Login user by using API POST method /users/login<br />

4- Add token by clicking authorize button (on right top on swagger) then paste token and authorize.<br />

5- Get user Profile by using API GET method /users/profile


## Swagger

This App is using swagger as a documentation for the APIs you can open swagger on following link
http://localhost:3000/api-docs



## Architecture

The idea was to make this app consist of three Microservices (API Gateway, Orders, Products) and 
use RabbitMQ to act as middleware (message broker between the microservices).<br />

I tried to follow the API gateway pattern to have an gateway that requests go through it and the gateway
decide to which microservice this request should go and to authorize the request and generate JWT token.
<br />
Pros of this approach:
<br />
1) remove the complexity of handling different microservices on different hosts from Frontend Applications
2) authenticate and authorize all request before forward it to the required service.
<br />

Cons of this approach:
<br />
1) add complexity to BE developers who handle API-Gateway service on changing happened on other services.
2) Increase response time as the request is checked (authenticate and authorized) then forwaded
<br />

Also using the RabbitMQ client to achieve the Choreography-based saga pattern by having local transaction publishes orders events that trigger local transactions in API-Gateway service and vice versa.

The architecture of the app are based on three nodes:

### API Gateway 

This service is responsible for handling the requests from the Frontend clients and do the authentication then forward the request to the required service.

### Product Service 

This service is responsible for handling the requests of Products.


### Order Service

This service is responsible for handling the requests of Orders.


## Test
1- Change ENV NODE_ENV on Docker file on projects (api_gateway/Dockerfile && order_service/Dockerfile && product_service/Dockerfile) to test

ENV NODE_ENV=test  <br />

comment on the 3 services the following by adding # so it will be like this:<br />
#CMD ["npm", "run", "start"]<br />

2- Build docker and run docker by these commands

```bash
$ docker-compose build
```

Then type into the terminal:

```bash
$ docker-compose up
```

Then after you see that the mysql is connected to port 3306 run the following:
```bash
docker-compose run api-gateway npm run dbcreate
docker-compose run api-gateway npm run dbmigrate

docker-compose run order-service npm run dbcreate
docker-compose run order-service npm run dbmigrate

docker-compose run product-service npm run dbcreate
docker-compose run product-service npm run dbmigrate
docker-compose run product-service npm run dbseed
```
Then run test:
```bash
docker-compose run api-gateway npm test
docker-compose run order-service npm test
docker-compose run product-service npm test
```


### Request Methods

As a rule of thumb, the request methods is as follows:
<br />

API-Gateway Service requests:
<br />
|Method|Description|link|
| ------ | ------ | ----- |
|post|Create new user|http://localhost:3000/users/register|
|post|Login user|http://localhost:3000/users/login|
|get|Get user profile|http://localhost:3000/users/profile|
|post|Get list of products based on user balance and search by filters|http://localhost:3000/products|
|get|Get Orders|http://localhost:3000/orders|
|post|Create new Order|http://localhost:3000/orders|
|post|Cancel Order|http://localhost:3000/orders/cancel|
<br />
<br />

Product Service requests:
<br />
|Method|Description|link|
| ------ | ------ | ----- |
|post|Get list of products based on price and search by filters|http://localhost:4000/products|
<br />
<br />

Order Service requests:
<br />
|Method|Description|link|
| ------ | ------ | ----- |
|get|Get Orders|http://localhost:5000/orders|
|post|Create new Order|http://localhost:5000/orders|
|post|Cancel Order|http://localhost:5000/orders/cancel|
