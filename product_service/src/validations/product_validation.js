const Joi = require("joi");

const get_products_validation = (data) => {
  const schema = Joi.object({
    filters: Joi.object().keys({
      name: Joi.string(),
      price: Joi.number(),
      category_id: Joi.number(),
      brand_id: Joi.number(),
    }),
  });

  return schema.validate(data);
};

module.exports.get_products_validation = get_products_validation;
