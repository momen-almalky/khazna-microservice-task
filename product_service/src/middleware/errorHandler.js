const constants = require("../helpers/constants");

function errorHandler(err, req, res, next) {
  if (err.type === constants.string) {
    return res.status(400).json({ message: err.message });
  }

  if (err.type === constants.UnauthorizedError) {
    return res.status(401).json({ message: "Invalid Token" });
  }

  if (err.type === constants.NotFound) {
    return res.status(404).json({ message: err.message });
  }

  if (err.type === constants.UnprocessEntity) {
    return res.status(422).json({ message: err.message });
  }

  
  return res.status(500).json({ message: err.message });
}
module.exports = errorHandler;
