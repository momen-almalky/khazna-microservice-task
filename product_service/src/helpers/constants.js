module.exports = {
  NotFound: "NotFound",
  string: "string",
  UnauthorizedError: "UnauthorizedError",
  UnprocessEntity: "UnprocessEntity",
  balanceDeductAction: "deduct",
  balanceAddAction: "add",
};
