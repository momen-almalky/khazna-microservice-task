"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert("Products", [
      {
        name: "Z3",
        price: 400.0,
        category_id: 1,
        brand_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Iphone 12",
        price: 900.0,
        category_id: 2,
        brand_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Apple Watch",
        price: 300.0,
        category_id: 2,
        brand_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Apple TV",
        price: 1000.0,
        category_id: 3,
        brand_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Water Dispenser",
        price: 1200.0,
        category_id: 3,
        brand_id: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Water Heater",
        price: 1200.0,
        category_id: 4,
        brand_id: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("Products", null, {});
  },
};
