'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert("Brands", [
      {
        name: "sony",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "apple",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "wansa",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "toshiba",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("Brands", null, {});
  }
};
