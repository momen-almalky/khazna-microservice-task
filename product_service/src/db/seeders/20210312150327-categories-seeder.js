"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert("Categories", [
      {
        name: "mobiles",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "electronics",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "home",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "basics",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Categories", null, {});
  },
};
