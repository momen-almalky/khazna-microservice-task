const { Model } = require("sequelize");
require("dotenv");

module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Brand, {foreignKey: 'brand_id', as: 'brand'});
      this.belongsTo(models.Category, {foreignKey: 'category_id', as: 'category'});
    }
  }

  Product.init(
    {
      name: DataTypes.STRING,
      price: DataTypes.DECIMAL(15, 2),
      category_id: DataTypes.INTEGER,
      brand_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Product",
    }
  );

  return Product;
};
