const { Product } = require("../../db/models");
require("dotenv");
const { Op } = require("sequelize");

/**
 * getList
 * get List of Products
 * @param {String} name
 * @param {Decimal} price
 * @param {Integer} category_id
 * @param {Integer} brand_id
 * @return {Array} Products
 */
module.exports.getList = async function ({
  name,
  price,
  category_id,
  brand_id,
}) {
  // query builder
  let query_builder = transformFiltersBuilder(
    name,
    price,
    category_id,
    brand_id
  );
  // select products with eager loading to brands and categories
  products = await Product.findAll({
    where: {
      [Op.and]: query_builder,
    },
    include: ["brand", "category"],
  });

  return products;
};

/**
 * transformFiltersBuilder
 * transform filters to query
 * @param {String} name
 * @param {Decimal} price
 * @param {Integer} category_id
 * @param {Integer} brand_id
 * @return {Array} builder
 */
function transformFiltersBuilder(name, price, category_id, brand_id) {
  let builder = [];
  if (name) {
    builder.push({ name: name });
  }
  if (price) {
    builder.push({ price: { [Op.lte]: price } });
  }
  if (category_id) {
    builder.push({ category_id: category_id });
  }
  if (brand_id) {
    builder.push({ brand_id: brand_id });
  }

  return builder;
}
