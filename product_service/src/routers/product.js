const express = require("express");
const {
  get_products_validation
} = require("../validations/product_validation");
require("dotenv");
const productService = require("./services/product_service");
const errorHandler = require("../middleware/errorHandler");
const CustomError = require("../helpers/CustomError");
const constants = require("../helpers/constants");

const router = new express.Router();

/**
 * @api {post} /products/ Get list of products based on filters
 * @apiName List products
 * @apiGroup products
 *
 * @apiParam  {object} filters
 *
 * @apiSuccess (200) {Array} mixed `{Products}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/products", async (req, res, next) => {
  try {
    const { body } = req;
    const { error } = get_products_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const products = await productService.getList(body.filters);

    res.status(200).send({ products: products });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

module.exports = router;
