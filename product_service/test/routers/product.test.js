const chai = require("chai");
const expect = chai.expect;
const invalid_product_sample = require("../samples/invalid_product_sample");
const product_user_sample = require("../samples/product_user_sample");
const request = require("supertest");
database = require("../../src/db/models/index");
server = require("../../index");

describe("product router", () => {
  before((done) => {
    require("../../src/db/models/index");
    done();
  });

  describe("POST /products", () => {
    it("should return 200", async () => {
      const res = await request(server)
        .post("/products")
        .send(product_user_sample);
      expect(res.status).eql(200);
    });
  });

  describe("POST /products", () => {
    it("should return 422", async () => {
      const res = await request(server)
        .post("/products")
        .send(invalid_product_sample);
      expect(res.status).eql(422);
    });
  });
});
