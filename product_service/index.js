const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
require("./src/db/models/index");
const errorHandler = require('./src/middleware/errorHandler');
const product_router = require("./src/routers/product");
dotenv.config();

const app = express();
const server = require("http").createServer(app);

app.use(bodyParser.json());
app.use(product_router);

// global error handler
app.use(errorHandler);

const port = process.env.PORT || 4000;
server.listen(port, () => {
  console.log("Server is up on port " + port);
});

module.exports = server;
