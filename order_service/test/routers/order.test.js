const chai = require("chai");
const expect = chai.expect;
const create_order_sample = require("../samples/create_order_sample");
const cancel_order_sample = require("../samples/cancel_order_sample");
const request = require("supertest");
database = require("../../src/db/models/index");
server = require("../../index");

describe("order router", () => {
  before((done) => {
    require("../../src/db/models/index");
    done();
  });

  describe("POST /orders", () => {
    it("should create order in database", async () => {
      const res = await request(server)
        .post("/orders")
        .send(create_order_sample);
      console.log(res);
      expect(res.status).eql(200);
    });
  });

  describe("POST /orders/cancel", () => {
    it("should cancel order", async () => {
      const res = await request(server)
        .post("/orders/cancel")
        .send(cancel_order_sample);
      expect(res.status).eql(200);
    });
  });
});
