const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
require("./src/db/models/index");
const errorHandler = require("./src/middleware/errorHandler");
const order_router = require("./src/routers/order");
const queue = require("./src/helpers/queue");
dotenv.config();

const app = express();
const server = require("http").createServer(app);

app.use(bodyParser.json());
app.use(order_router);

// global error handler
app.use(errorHandler);

const port = process.env.PORT || 5000;
server.listen(port, () => {
  console.log("Server is up on port " + port);
});

const intervalId = setInterval(() => {
  queue.subscribeToUserQueue().then((isConnected) => {
    if (isConnected) {
      clearInterval(intervalId);
      console.log("UserQueue is up:");
    }
  });
}, 20000);

module.exports = server;
