module.exports = {
  NotFound: "NotFound",
  string: "string",
  UnauthorizedError: "UnauthorizedError",
  UnprocessEntity: "UnprocessEntity",
  balanceDeductAction: "deduct",
  orderCreated: "created",
  paymentApproved: "payment_approved",
  paymentDeclined: "payment_declined",
  orderPreparing: "preparing",
  orderDelivered: "delivered",
  orderCancelled: "cancelled",

  orderCreatedEvent: "order_created_event",
  orderCancelledEvent: "order_cancelled_event",
  paymentApprovedEvent: "payment_approved_event",
  paymentDeclinedEvent: "payment_declined_event",
};
