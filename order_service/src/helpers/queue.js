const amqp = require("amqplib");
const EventEmitter = require("events");
require("dotenv");
const constants = require("../helpers/constants");
const orderService = require("../routers/services/order_service");
const CustomError = require("../helpers/CustomError");

const produce = async (queue, message, durable = false, persistent = false) => {
  const connect = await amqp.connect(process.env.RABBIT_MQ_URL);
  const channel = await connect.createChannel();

  await channel.assertQueue(queue, { durable });
  await channel.sendToQueue(queue, Buffer.from(message), { persistent });
};

const consume = async (
  queue,
  isNoAck = false,
  durable = false,
  prefetch = null
) => {
  const connect = await amqp.connect(process.env.RABBIT_MQ_URL);
  const channel = await connect.createChannel();

  await channel.assertQueue(queue, { durable });

  if (prefetch) {
    channel.prefetch(prefetch);
  }
  const consumeEmitter = new EventEmitter();
  try {
    channel.consume(
      queue,
      (message) => {
        if (message !== null) {
          content = JSON.parse(message.content.toString());
          consumeEmitter.emit(content.event, content, () =>
            channel.ack(message)
          );
        } else {
          const error = new CustomError(
            constants.string,
            "NullMessageException"
          );
          consumeEmitter.emit("error", error);
        }
      },
      { noAck: isNoAck }
    );
  } catch (error) {
    consumeEmitter.emit("error", error);
  }
  return consumeEmitter;
};

const subscribeToUserQueue = async () => {
  try {
    const consumeEmmitter = await consume(process.env.ORDERS_QUEUE);

    consumeEmmitter.on(constants.paymentApprovedEvent, async (order, ack) => {
      await orderService.changeStatus({
        order_id: order.data.id,
        status: constants.paymentApproved,
      });
    });

    consumeEmmitter.on(constants.paymentDeclinedEvent, async (order, ack) => {
      await orderService.changeStatus({
        order_id: order.data.id,
        status: constants.paymentDeclined,
      });
    });
    consumeEmmitter.on("error", (error) => console.error(error));
    return true;
  } catch (error) {
    console.log(error);
  }
};
module.exports = {
  produce,
  consume,
  subscribeToUserQueue,
};
