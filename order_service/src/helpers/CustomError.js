class CustomError extends Error {
  constructor(type = "string", ...params) {
    super(...params);

    // Maintains proper stack trace for where our error was thrown
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError);
    }

    this.name = "CustomError";
    // Custom debugging information
    this.type = type;
  }
}
module.exports = CustomError;
