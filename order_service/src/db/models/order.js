"use strict";
const { Model } = require("sequelize");
const constants = require("../../helpers/constants");

module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.OrderProducts, {
        foreignKey: "order_id",
        as: "products",
      });
    }
  }
  Order.init(
    {
      status: {
        type: DataTypes.ENUM,
        values: [
          constants.orderCreated,
          constants.paymentApproved,
          constants.paymentDeclined,
          constants.orderPreparing,
          constants.orderDelivered,
          constants.orderCancelled,
        ],
      },
      total: DataTypes.DECIMAL(15, 2),
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Order",
    }
  );
  return Order;
};
