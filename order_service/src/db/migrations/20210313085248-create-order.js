"use strict";
const constants = require("../../helpers/constants");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Orders", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.ENUM(
          constants.orderCreated,
          constants.paymentApproved,
          constants.paymentDeclined,
          constants.orderPreparing,
          constants.orderDelivered,
          constants.orderCancelled
        ),
      },
      total: {
        type: Sequelize.DECIMAL(15, 2),
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Orders");
  },
};
