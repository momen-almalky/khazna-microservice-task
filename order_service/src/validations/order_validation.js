const Joi = require("joi");

const request_products_validation = (data) => {
  const schema = Joi.object({
    products: Joi.array()
      .min(1)
      .items(
        Joi.object({
          id: Joi.number(),
          price: Joi.number(),
        })
      ),
    user_id: Joi.number(),
  });

  return schema.validate(data);
};

const get_orders_validation = (data) => {
  const schema = Joi.object({
    user_id: Joi.number(),
  });

  return schema.validate(data);
};

const cancel_order_validation = (data) => {
  const schema = Joi.object({
    order_id: Joi.number(),
  });

  return schema.validate(data);
};

module.exports.request_products_validation = request_products_validation;
module.exports.get_orders_validation = get_orders_validation;
module.exports.cancel_order_validation = cancel_order_validation;
