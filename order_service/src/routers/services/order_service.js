const { Order, OrderProducts } = require("../../db/models");
require("dotenv");
const constants = require("../../helpers/constants");
const sequelize = require("../../db/models").sequelize;
const { Op } = require("sequelize");
const CustomError = require("../../helpers/CustomError");
const queue = require("../../helpers/queue");

/**
 * create
 * Create new order
 * @param {Array} products
 * @param {Integer} user_id
 * @return {Object} order
 */
module.exports.create = async function ({ products, user_id }) {
  const transaction = await sequelize.transaction();

  try {
    let total = 0;
    let products_ids = [];
    products.forEach((product) => {
      total += product.price;
      products_ids.push({ product_id: product.id });
    });

    const order = await Order.create(
      {
        user_id: user_id,
        total: total,
        status: constants.orderCreated,
      },
      { transaction }
    );

    products_ids.forEach((product) => {
      product.order_id = order.id;
    });

    await OrderProducts.bulkCreate(products_ids, { transaction });
    await transaction.commit();

    await queue.produce(
      process.env.USER_QUEUE,
      JSON.stringify({ event: constants.orderCreatedEvent, data: order })
    );

    return order;
  } catch (error) {
    await transaction.rollback();
    throw new CustomError(constants.string, `Couldn't Create Order`);
  }
};

/**
 * getList
 * Get List of user's orders
 * @param {Integer} user_id
 * @return {Array} order
 */
module.exports.getList = async function ({ user_id }) {
  orders = await Order.findAll({
    where: {
      user_id: user_id,
    },
    include: ["products"],
  });

  return orders;
};

/**
 * cancel
 * Cancel user's order by order_id
 * @param {Integer} order_id
 * @return {Boolean} done
 */
module.exports.cancel = async function ({ order_id }) {
  let order = await Order.findOne({
    where: {
      id: order_id,
    },
  });

  if (!order) {
    throw new CustomError(constants.NotFound, `Order not found`);
  }

  if (order.status == constants.orderDelivered) {
    throw new CustomError(constants.string, `Order is already delivered`);
  }

  order.status = constants.orderCancelled;
  await order.save();

  await queue.produce(
    process.env.USER_QUEUE,
    JSON.stringify({ event: constants.orderCancelledEvent, data: order })
  );
  return true;
};

/**
 * change status
 * Change status of order by order_id
 * @param {Integer} user_id
 * @param {Integer} order_id
 * @return {Boolean} done
 */
module.exports.changeStatus = async function ({ order_id, status }) {
  let order = await Order.findOne({
    where: {
      id: order_id,
    },
  });

  if (!order) {
    throw new CustomError(constants.NotFound, `Order not found`);
  }

  if (order.status == constants.orderDelivered) {
    throw new CustomError(constants.string, `Order is already delivered`);
  }

  order.status = status;
  await order.save();

  return true;
};
