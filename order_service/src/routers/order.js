const express = require("express");
const {
  request_products_validation,
  get_orders_validation,
  cancel_order_validation,
} = require("../validations/order_validation");
require("dotenv");
const order_service = require("./services/order_service");
const errorHandler = require("../middleware/errorHandler");
const CustomError = require("../helpers/CustomError");
const constants = require("../helpers/constants");

const router = new express.Router();

/**
 * @api {post} /orders/ Create new Order
 * @apiName Create Order
 * @apiGroup orders
 *
 * @apiParam  {Array} products
 * @apiParam  {Integer} user_id
 *
 * @apiSuccess (200) {Object} mixed `{Order}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/orders", async (req, res, next) => {
  try {
    const { body } = req;
    const { error } = request_products_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const order = await order_service.create(body);

    res.status(200).send({ order: order });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {get} /orders/ Get list of orders history
 * @apiName Get Order List
 * @apiGroup orders
 *
 * @apiParam  {Integer} user_id
 *
 * @apiSuccess (200) {Array} mixed `{Orders}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.get("/orders", async (req, res, next) => {
  try {
    const { query } = req;
    const { error } = get_orders_validation(query);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    const orders = await order_service.getList(query);

    res.status(200).send({ orders: orders });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

/**
 * @api {post} /orders/cancel Cancel Order
 * @apiName Cancel Order
 * @apiGroup orders
 *
 * @apiParam  {Integer} user_id
 * @apiParam  {Integer} order_id
 *
 * @apiSuccess (200) {Object} mixed `{Order}` object
 * @apiFailure (400) {Object} mixed `err` object
 */
router.post("/orders/cancel", async (req, res, next) => {
  try {
    const { body } = req;
    const { error } = cancel_order_validation(body);

    if (error) {
      throw new CustomError(
        constants.UnprocessEntity,
        error.details[0].message
      );
    }

    await order_service.cancel(body);

    res.status(200).send({ message: "Order is cancelled" });
  } catch (error) {
    console.error(error);
    errorHandler(error, req, res, next);
  }
});

module.exports = router;
