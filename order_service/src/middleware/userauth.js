const jwt = require("jsonwebtoken");
const { User } = require("../db/models");
require("dotenv");
const errorHandler = require("../middleware/errorHandler");
const CustomError = require("../helpers/CustomError");
const constants = require("../helpers/constants");

const userauth = async (req, res, next) => {
  try {
    const auth_header = req.header("Authorization");
    if (!auth_header) {
      throw new CustomError(constants.UnauthorizedError);
    }
    const token = auth_header.replace("Bearer ", "");

    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const user = await User.findOne({
      where: { id: decoded.id, token: token },
    });

    if (!user) {
      throw new CustomError(constants.UnauthorizedError);
    }

    req.token = token;
    req.user = user;
    next();
  } catch (error) {
    errorHandler(error, req, res, next);
  }
};

module.exports = userauth;
